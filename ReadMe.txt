The Read me for this intelliJ (11 community edition) example combining RoboGuice and Robolectric
Please reference this directly instead of spinning something else up at Git (or where ever).  Thanks in advance!
The idea here is to provide everyone, and myself, an example of how to get the two to place nicely without all flagging on and off of which items are tests items and allow for larger flexibility of projects. You know using IntelliJ's modules as they were intended.
Like many of you all, I like IntelliJ when dealing with Java based development. Equally Roboguice seems to be a good candidate for dependency injection. Unfortunately, the documentation for it is lacking heavily. It's taken quite a bit of energy in the past to get up and running where I feel comfortable with it.
I've recently encountered a situation where upgrading isn't optional; it's required. This is where Robolectric comes in. 

Specs:

Ide IntelliJ: http://www.jetbrains.com/idea/

Note: I'm not a maven user, so you'll see just bare-bones jar references.

Roboguice can be found at: https://code.google.com/p/roboguice/
Guice 3 (needed for Roboguice) can be found at: http://code.google.com/p/google-guice/
Robolectric can be found at: http://pivotal.github.com/robolectric/
JUnit (needed for Robolectric) can be found at: http://www.junit.org/ 
As for the specifics you can find how to get setup in more detail at the respective frameworks' sites (Roboguice and Robolectric)

TheApp points to refernce (Module SDK) Android Platform sdk 2.2 and also references ()android-support-v4.jar addition

In my case 1.1 build of Robolectric worked with me. I had issues with 1.2, but it could be that I needed a later version of JUnit for the assist.

Also you may have issues between the two like I did a while back:
http://stackoverflow.com/questions/8761020/attempting-to-unit-test-and-something-with-roboguice-newdefaultrobomodule-is-f
Just make sure you include the proper library/jar file.

Also, to get around the issue where it can't find the androidmanifest, I redirected based on all modules being sister folders. You can see this in the CustomTestRunner.Java in "TheTest". Also Robolectric discusses how to change this and others at: http://pivotal.github.com/robolectric/customizing.html

Also when you run the test(s), you'll probably see the following output:

Warning: an error occurred while binding shadow class: ShadowGeoPoint
Warning: an error occurred while binding shadow class: ShadowItemizedOverlay
Warning: an error occurred while binding shadow class: ShadowMapController
Warning: an error occurred while binding shadow class: ShadowMapActivity
Warning: an error occurred while binding shadow class: ShadowMapView
Warning: an error occurred while binding shadow class: ShadowNdefMessage
Warning: an error occurred while binding shadow class: ShadowNdefRecord
Warning: an error occurred while binding shadow class: ShadowNfcAdapter
Warning: an error occurred while binding shadow class: ShadowOverlayItem

Don't worry, this is just saying maps info is included. In my example, I'm not using it. I'm sure if you need it both Roboguice and Robolectric discuss how to implement it, which I'll need to figure this one out too! :)

Also, I've run a simple signed export using the proguard and have not seen problems. Of course project is incredibly simple.

With all of this said, this isn't a defacto standard, the is just something to help everyone get going. Use everything at your own risk.

Cheers,
Kelly
website@cubedelement.com
