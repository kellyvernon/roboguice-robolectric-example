package com.cubedelement.module;

import com.cubedelement.binders.ILoad;
import com.cubedelement.binders.TheLoad;
import com.google.inject.AbstractModule;

/**
 * Created with IntelliJ IDEA.
 * User: Kelly Vernon
 * Date: 9/22/12
 * Time: 9:06 PM
 */
public class CubedElementModule extends AbstractModule {
    @Override
    protected void configure() {
          bind(ILoad.class).to(TheLoad.class);
    }
}
