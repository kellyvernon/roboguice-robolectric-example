package com.cubedelement.binders;

/**
 * Created with IntelliJ IDEA.
 * User: Kelly Vernon
 * Date: 9/22/12
 * Time: 9:13 PM
 */
public class TheLoad implements ILoad{
    @Override
    public String doSomething() {
        return "I've been loaded";
    }
}
