package com.cubedelement.binders;

/**
 * Created with IntelliJ IDEA.
 * User: Kelly Vernon
 * Date: 9/22/12
 * Time: 9:12 PM
 */
public interface ILoad {
    String doSomething();
}
