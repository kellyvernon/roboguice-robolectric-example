package com.cubedelement;

import android.os.Bundle;
import android.widget.TextView;
import com.cubedelement.binders.ILoad;
import com.google.inject.Inject;
import roboguice.activity.RoboActivity;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

@ContentView(R.layout.main)
public class CubedElementActivity extends RoboActivity {

    @InjectView(R.id.test)
    TextView textTest;

    @Inject
    public ILoad aLoader;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        textTest.setText(aLoader.doSomething());
    }
}
