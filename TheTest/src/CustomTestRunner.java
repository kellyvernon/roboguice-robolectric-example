import com.xtremelabs.robolectric.RobolectricTestRunner;
import org.junit.runners.model.InitializationError;

import java.io.File;

/**
 * Created with IntelliJ IDEA.
 * User: Kelly Vernon
 * Date: 9/22/12
 * Time: 8:57 PM
 */
public class CustomTestRunner extends RobolectricTestRunner {
    public CustomTestRunner(Class testClass) throws InitializationError {
        super(testClass, new File("./TheApp/"));
    }
}