/**
 * Created with IntelliJ IDEA.
 * User: Kelly Vernon
 * Date: 9/22/12
 * Time: 8:53 PM
 */

import com.cubedelement.CubedElementActivity;
import com.cubedelement.R;
import org.junit.Test;
import org.junit.runner.RunWith;
import roboguice.RoboGuice;
import roboguice.activity.RoboActivity;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

@RunWith(CustomTestRunner.class)
public class CubedElementActivityTest {

    @Test
    public void shouldHaveHappySmiles() throws Exception {
        String appName = new CubedElementActivity().getResources().getString(R.string.app_name);
        assertThat(appName, equalTo("TheSuperApp"));
    }

    @Test
    public void shouldHaveLoad() throws Exception {
        RoboActivity activity = new RoboActivity();
        String doSomething = RoboGuice.getInjector(activity).getInstance(CubedElementActivity.class).aLoader.doSomething();
        assertThat(doSomething, equalTo("I've been loaded"));
    }
}
